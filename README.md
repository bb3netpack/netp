# NetP #

### What is this repository for? ###

* NetP is a Beepbeep-3 palette for network packets manipulation.
* Version 1.0

### How do I get set up? ###

* Download the repository sources
* Download Beepbeep-3 here: https://github.com/liflab/beepbeep-3
* Download jNetPcap here: http://jnetpcap.com/
* Learn Beepbeep-3 here: http://liflab.github.io/beepbeep-3/index.html
* Now you're ready to use NetP